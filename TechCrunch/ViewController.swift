//
//  ViewController.swift
//  TechCrunch
//
//  Created by MARIANA CUADRADO on 15/6/17.
//  Copyright © 2017 MARIANA CUADRADO. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {


    @IBOutlet weak var articulosTableView: UITableView!

    var articles: [Articulos]? = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buscarArticulos()
        
    }

    func buscarArticulos(){
        
        let urlRequest = URLRequest(url: URL(string: "https://newsapi.org/v1/articles?source=techcrunch&sortBy=top&apiKey=0cec6a81dd0e4df385ca611eafcaf849")!)
        
        let task = URLSession.shared.dataTask(with: urlRequest){(data, response, error) in
            
            if error != nil {
                print(error)
                return
            }
            
            self.articles = [Articulos] ()
            
            do{
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String : AnyObject]
                
                if let articulosJson = json["articles"] as? [[String : AnyObject]] {
                    for articulos in articulosJson{
                        
                        let articulo = Articulos()
                        
                        if let tituloArt = articulos["title"] as? String, let autor = articulos["author"] as? String, let descripcionArt = articulos["description"] as? String, let urlArt = articulos["url"] as? String, let urlImg = articulos["urlToImage"] as? String{
                            
                            articulo.autor = autor
                            articulo.descripcion = descripcionArt
                            
                            articulo.titulo = tituloArt
                            articulo.url = urlArt
                            articulo.imageUrl = urlImg
                            
                        }
                        
                        self.articles?.append(articulo)
                    
                    }
                    
                    
                    
                }
                
                DispatchQueue.main.sync {
                    self.articulosTableView.reloadData()
                }
                
                
            }
            catch let  error{
                print(error)
            }
        }
        task.resume()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = articulosTableView.dequeueReusableCell(withIdentifier: "articuloCelda", for: indexPath) as! ArticulosTableViewCell
        
        cell.tituloLabel.text = self.articles?[indexPath.item].titulo
        cell.descripcionLabel.text = self.articles?[indexPath.item].descripcion
        cell.autorLabel.text = self.articles?[indexPath.item].autor
        cell.fotoImageView.downloadImage(from: (self.articles?[indexPath.item].imageUrl)!)
        
        
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.articles?.count  ?? 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let webvc = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "web") as! DetalleViewController
    
        
        webvc.url = self.articles?[indexPath.item].url
        
        
        self.present(webvc, animated: true, completion: nil)
    }
    
}

extension UIImageView{
    func downloadImage(from url: String){
        
        let urlRequest = URLRequest(url: URL(string: url)!)
        
        let task = URLSession.shared.dataTask(with: urlRequest){(data, response, error) in
            
            if error != nil {
                print(error)
                return
            }
            
            DispatchQueue.main.sync {
                self.image = UIImage(data: data!)
            }

            
        }
        task.resume()
    }

    
    }


