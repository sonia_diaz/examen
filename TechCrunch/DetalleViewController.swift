//
//  DetalleViewController.swift
//  TechCrunch
//
//  Created by MARIANA CUADRADO on 15/6/17.
//  Copyright © 2017 MARIANA CUADRADO. All rights reserved.
//

import UIKit

class DetalleViewController: UIViewController {


    @IBOutlet weak var articleweb: UIWebView!
  
    
    var url: String?
  
    
    override func viewDidLoad() {
        super.viewDidLoad()

        articleweb.loadRequest(URLRequest(url: URL(string: url!)!))
        
        
    }
    //

    
    
    
}
