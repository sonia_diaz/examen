//
//  Articulos.swift
//  TechCrunch
//
//  Created by MARIANA CUADRADO on 15/6/17.
//  Copyright © 2017 MARIANA CUADRADO. All rights reserved.
//

import UIKit

class Articulos: NSObject {
    
    var titulo: String?
    var descripcion: String?
    var autor: String?
    var url: String?
    var imageUrl: String?

}
