//
//  ArticulosTableViewCell.swift
//  TechCrunch
//
//  Created by MARIANA CUADRADO on 15/6/17.
//  Copyright © 2017 MARIANA CUADRADO. All rights reserved.
//

import UIKit

class ArticulosTableViewCell: UITableViewCell {

    
    @IBOutlet weak var fotoImageView: UIImageView!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var descripcionLabel: UILabel!
    @IBOutlet weak var autorLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
